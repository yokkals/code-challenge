var assert = require('assert');

const mongoose = require('../db/mongoose');
const Book = require('../models/Book')(mongoose);
const Institution = require('../models/Institution')(mongoose);
const User = require('../models/User')(mongoose);

var testBook;
var testUser;
const testUserPassword = 'testPword123';

beforeEach(function() {
	testBook = new Book({_id:'xyz7654', title:'Book Title Here', isbn:'1234567890123', author:'Bob Jones'});
	testUser = new User({_id:'1234abc', name:'Test User', email:'testuser@gmail.com'});
});

describe('User', function() {
	describe('#setPassword / #validatePassword', function() {
		it('should validate a correct password', function() {
			testUser.setPassword(testUserPassword);
			assert.equal(testUser.validatePassword(testUserPassword), true);
		});
	});
	describe('#setPassword / #validatePassword - Negative', function() {
		it('should invalidate an incorrect password', function() {
			testUser.setPassword(testUserPassword);
			assert.equal(testUser.validatePassword('wrongPassword'), false);
		});
	});
	describe('#toAuthJSON', function() {
		it('should return correct object with _id, email, and token', function() {
			var obj = testUser.toAuthJSON();
			assert.equal(obj._id, testUser._id);
			assert.equal(obj.email, testUser.email);
			assert.equal(obj.token, testUser.generateJWT());
		});
	});
});

describe('Book', function() {
	describe('#toRestItem', function() {
		it('should return correct object', function() {
			var obj = testBook.toRestItem();
			assert.equal(obj._id, testBook._id);
			assert.equal(obj.isbn, testBook.isbn);
			assert.equal(obj.title, testBook.title);
			assert.equal(obj.author, testBook.author);
		});
	});
});
