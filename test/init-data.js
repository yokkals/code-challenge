const mongoose = require('../db/mongoose');
const User = require('../models/User')(mongoose);
const Inst = require('../models/Institution')(mongoose);
const Book = require('../models/Book')(mongoose);

/*
  Script for initializing data -- namely, institutions and books.
*/

createInsts();

createBooks();

var insts = [];
function createInsts() {
	new Inst({name:'Test Institution 1', emailDomain:'testinst.edu'}).save().then((inst) => insts.push(inst));
	new Inst({name:'Another Inst', emailDomain:'another.edu'}).save().then((inst) => insts.push(inst));
	new Inst({name:'And a Third One', emailDomain:'third.edu'}).save().then((inst) => insts.push(inst));
}

var books = [];
function createBooks() {
	setTimeout(function() {
		new Book({isbn:'12345', title:'First Book', instIds: [insts[0]._id]}).save().then((book) => books.push(book));
		new Book({isbn:'23456', title:'Test Book 2', instIds: [insts[1]._id, insts[2]._id]}).save().then((book) => books.push(book));
		new Book({isbn:'98765', title:'Third Test Book', instIds: [insts[2]._id]}).save().then((book) => books.push(book));
	}, 3000);
}

// book[0] --> testinst.edu
// book[1] --> another.edu, third.edu
// book[2] --> third.edu
