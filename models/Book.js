module.exports = function(mongoose) {
	try {
		return mongoose.model('Book');
	} catch (err) {
		
		const BookSchema = new mongoose.Schema({
			id: mongoose.Schema.ObjectId,
			isbn: String,
			title: String,
			author: String,
			instIds: [String]
		});
		
		
		BookSchema.methods.toRestItem = function() {
			return {
				_id: this._id,
				isbn: this.isbn,
				title: this.title,
				author: this.author
			};
		};
		
		return mongoose.model('Book', BookSchema);
		
	}
}