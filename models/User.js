const crypto = require('crypto');
const jwt = require('jsonwebtoken');

module.exports = function(mongoose) {
	try {
		return mongoose.model('User');
	} catch (err) {
		
		const UserSchema = new mongoose.Schema({
			id: mongoose.Schema.ObjectId,
			name: String,
			email: String,
			role: String,
			passHash: String,
			passSalt: String,
			instId: String
		});
		
		UserSchema.methods.setPassword = function(password) {
			this.passSalt = crypto.randomBytes(16).toString('hex');
			this.passHash = crypto.pbkdf2Sync(password, this.passSalt, 10000, 512, 'sha512').toString('hex');
		};

		UserSchema.methods.validatePassword = function(password) {
			const passHash = crypto.pbkdf2Sync(password, this.passSalt, 10000, 512, 'sha512').toString('hex');
			return this.passHash === passHash;
		};

		UserSchema.methods.generateJWT = function() {
			const today = new Date();
			const expirationDate = new Date(today);
			expirationDate.setDate(today.getDate() + 60);
			
			return jwt.sign({
				email: this.email,
				id: this._id,
				exp: parseInt(expirationDate.getTime() / 1000, 10),
			}, 'secret');
		}

		UserSchema.methods.toAuthJSON = function() {
			return {
				_id: this._id,
				email: this.email,
				token: this.generateJWT(),
			};
		};
		
		return mongoose.model('User', UserSchema);
		
	}
}