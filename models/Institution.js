module.exports = function(mongoose) {
	try {
		return mongoose.model('Institution');
	} catch (err) {
		
		return mongoose.model('Institution', {
			id: mongoose.Schema.ObjectId,
			name: String,
			emailDomain: String,
			url: String
		});
		
	}
}