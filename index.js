const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require('./config/passport');
const auth = require('./routes/auth');

const signin = require('./routes/users/signin.js');
app.post('/users/signin', auth.optional, signin);

const create = require('./routes/users/create.js');
app.post('/users/create', auth.optional, create);

const books = require('./routes/books.js');
app.get('/books', auth.required, books);

app.listen(3000, () => console.log(`Open http://localhost:3000 to see a response.`));
