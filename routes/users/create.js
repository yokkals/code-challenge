const mongoose = require('../../db/mongoose');
const User = require('../../models/User')(mongoose);
const Inst = require('../../models/Institution')(mongoose);

/* POST /users/create

	Requires a request body in the format:
	user: {
		email: (String),
		password: (String)
	}
*/
module.exports = (req, res, next) => {
	const requestUser = req.body.user;
	
	// TODO: validate fields (email exists, pw exists, etc)
	// TODO: user role (need to enforce using enum?)
	
	const user = new User(requestUser);
	
	const domain = getEmailDomain(user.email);
	
	Inst.findOne({emailDomain: domain}).then(inst => {
		if(inst) {
			user.instId = inst._id;
			user.setPassword(requestUser.password);
			
			user.save()
				.then(user => {
					res.status(201).json({ status: "success", data: {user: user.toAuthJSON()} });
				})
				.catch(err => {
					res.status(500).send({ status: "error", message: "Error creating user", data: {err} });
				});
		
		} else {
			res.status(412).send({ status: "fail", data: {"institution":  "An institution with email domain ["+domain+"] was not found."} });
		}
	});
}

function getEmailDomain(emailAddress) {
	return emailAddress.substring(emailAddress.indexOf("@") + 1);
}