const mongoose = require('../../db/mongoose');
const passport = require('passport');
const auth = require('../auth');
const User = require('../../models/User')(mongoose);
	
/* POST /users/signin

	Requires a request body in the format:
	user: {
		email: (String),
		password: (String)
	}
*/
module.exports = (req, res, next) => {
	const { body: { user } } = req;
	
	return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
		if(err) {
			return next(err);
		}

		if(passportUser) {
			const user = passportUser;
			user.token = passportUser.generateJWT();

			return res.status(200).json({ status: "success", data: {user: user.toAuthJSON()} });
		}

		return res.status(400).json({ status: "fail", data: {info} });
	})(req, res, next);
	
}
