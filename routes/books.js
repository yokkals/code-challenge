const mongoose = require('../db/mongoose');
const Book = require('../models/Book')(mongoose);
const Institution = require('../models/Institution')(mongoose);
const User = require('../models/User')(mongoose);
	
module.exports = (req, res, next) => {
	
	const userId = req.payload.id;
	
	User.findById(userId)
		.then(user => {
			if(user) {
				
				Institution.findById(user.instId)
					.then(inst => {
						if(inst) {
							
							// TODO: try selecting only the fields we need (might not need to convert each item and it would save on payload/download for the query)
							
							// TODO: any paging/sorting considerations?
							
							Book.find({instIds: inst._id}).then(books => {
								var list = [];
								books.forEach(function(book) {
									list.push(book.toRestItem());
								});
								res.status(200).json({ status: "success", data: {books: list} });
							}).catch(err => {
								res.status(500).json({ status: "error", message: "Error finding books for institution " + inst._id, data: {err} });
							});
							
						} else {
							res.status(412).json({ status: "fail", data: {error:"Institution not found for ID: " + user.instId} });
						}
					})
					.catch(err => {
						res.status(500).json({ status: "error", message: "Error finding institution " + user.instId, data: {err} });
					});
				
			} else {
				res.status(412).json({ status: "fail", data: {error:"User not found: " + userId} });
			}
		})
		.catch(err => {
			res.status(500).json({ status: "error", message: "Error finding user " + userId, data: {err} });
		});
	
}
